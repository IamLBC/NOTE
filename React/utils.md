
```js
import {useEffect,useRef} from 'react';
export function useWatch(value,callback){
  const oldValue = useRef();
  const isInit = useRef(false);
  useEffect(() =>{
    if(!isInit.current){
      isInit.current = true;
    }else{
      callback(value,oldValue.current);
    }
    oldValue.current=value;
  },[value])
}
```

### 复制代码 再添加immediate配置来控制在组件初次渲染后是否马上执行callback回调函数。
```js
import {useEffect,useRef} from 'react';
export function useWatch(value,callback,config={immediate: false}){
  const oldValue = useRef();
  const isInit = useRef(false);
  useEffect(() =>{
    if(!isInit.current){
      isInit.current = true;
      if(config.immediate){
        callback(value,oldValue.current);
      }
    }else{
      callback(value,oldValue.current);
    }
    oldValue.current=value;
  },[value])
}
```

### 复制代码 另外Vue的watch还返回一个unwatch函数，调用unwatch函数可以停止监听该数据。
```js
import { useEffect, useRef } from 'react';
export function useWatch(value, callback, config = { immediate: false }) {
  const oldValue = useRef();
  const isInit = useRef(false);
  const isWatch = useRef(true);
  useEffect(() => {
    if (isWatch.current) {
      if (!isInit.current) {
        isInit.current = true;
        if (config.immediate) {
          callback(value, oldValue.current);
        }
      } else {
        callback(value, oldValue.current);
      }
      oldValue.current = value;
    }
  }, [value])

  const unwatch =  () => {
    isWatch.current = false;
  };
  return unwatch;
}
```

### 复制代码 useWatch 这个Hook 定义好后，这么使用。
```js
export {useState} from 'react';
export {useWatch} from './hook.js';
export default function HelloWorld() {
  const [title,setTitle] = useState('hello world')
  useWatch(title, (value, oldValue) => {
    console.log(value);
    console.log(oldValue)
  })
  const handleChangeTitle = () => {
    setTitle('hello React')
  }
  return (
    <div onClick={handleChangeTitle}>{title}</div>
  );
}

```