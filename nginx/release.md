## 发布流程

```bash
    # 切换到源码目录,对应在jenkins-home的workspace下面

    image_version=`date +%Y%m%d%H%M`;

    echo "清理已有容器及镜像资源"
    container="tenantsystemfront"
    filename="tenantSystemFront"
    port=4000
    containerport=80
    if docker ps | grep ${container} ;then
        docker stop ${container}
    fi

    if docker ps -a | grep ${container};then
        docker rm ${container}
    fi

    if docker images | grep ${container};then
        docker rmi $(docker images|grep ${container}|awk '{print $1 ":" $2}')
    fi

    # cd /var/jenkins_home/jobs/${filename}/workspace;

    docker build -t ${container}:${image_version} -f Dockerfile .

    docker cp  ${container}:/app/dist /data/tenantsystem_frontdata/

    docker ps -a

    docker run -t -d -p ${port}:${containerport} -v /data/tenantsystem_frontdata:/app --name ${container} ${container}:${image_version}

    docker logs ${container};

    docker image prune -af  --filter="dangling=true"
```

### Docker
```bash
    # 切换到源码目录,对应在jenkins-home的workspace下面

    image_version=`date +%Y%m%d%H%M`;

    echo "清理已有容器及镜像资源"
    container="tenantsystemfront"
    filename="tenantSystemFront"
    port=4000
    containerport=80
    if docker ps | grep ${container} ;then
        docker stop ${container}
    fi

    if docker ps -a | grep ${container};then
        docker rm ${container}
    fi

    if docker images | grep ${container};then
        docker rmi $(docker images|grep ${container}|awk '{print $1 ":" $2}')
    fi

    # cd /var/jenkins_home/jobs/${filename}/workspace;

    docker build -t ${container}:${image_version} -f Dockerfile .

    docker ps -a

    docker run -t --name ${container} nginx:latest -d -p ${port}:${containerport} -v /root/tenantsystem_frontdata:/etc/nginx 

    docker cp default.conf ${container}:/etc/nginx/conf.d 

    docker logs ${container};

    docker image prune -af  --filter="dangling=true"


    # 挂载目录                    主机端口:容器端口
    # docker run --name tenantsystem -d -p 4000:80 -v /data/tenantsystemfront_data:/etc/nginx nginx


    # 可通过以下命令进行查看，查看文件是否被挂载成功
    docker exec -it tenantSystem bash
    cd /etc/nginx 
    ls
```

### Dockerfile
```bash
    FROM node:10.15.2-alpine
    COPY package.json .
    RUN rm -rf node_modules/ \
        && npm install \
        && npm install -g serve 
    RUN npm run build
    # RUN ls
    # RUN pwd
    # COPY ./dist /root/data/tenantsystem_frontdata
    # WORKDIR /etc/nginx

    # EXPOSE 80
    # CMD ["serve", "-s", "./dist"]
```


### default.conf
```bash
server {
    listen 80;
    # gzip config
    gzip on;
    gzip_min_length 1k;
    gzip_comp_level 9;
    gzip_types text/plain text/css text/javascript application/json application/javascript application/x-javascript application/xml;
    gzip_vary on;
    gzip_disable "MSIE [1-6]\.";

    root /etc/nginx/dist;

    location / {
        root   /etc/nginx/dist;  # nginx容器被挂载路径，与主机挂载目录对应。
        index  index.html index.htm; # 被访问文件
        try_files $uri $uri/ /index.html;
    }
}
```
