
### 过滤
```js
const course = {
    math: 80,
    english: 85,
    chinese: 90
}
const res = Object.entries(course).filter(([key, val]) => val > 80)
console.log(res) // [ [ 'english', 85 ], [ 'chinese', 90 ] ]
console.log(Object.fromEntries(res)) // { english: 85, chinese: 90 }

Object.fromEntries(res.map(item => Object.entries(item)[0]))
```

### 对象 - 对象数组 互转
```js
const course = {
    math: 80,
    english: 85,
    chinese: 90
}
const res = Object.entries(course).map(([key, val]) => ({[key]: val}))

Object.fromEntries(res.map(item => Object.entries(item)[0]))
```

### url的search参数转换
```js
// let url = "https://www.baidu.com?name=jimmy&age=18&height=1.88"
// queryString 为 window.location.search
const queryString = "?name=jimmy&age=18&height=1.88";
const queryParams = new URLSearchParams(queryString);
const paramObj = Object.fromEntries(queryParams);
console.log(paramObj); // { name: 'jimmy', age: '18', height: '1.88' }
```